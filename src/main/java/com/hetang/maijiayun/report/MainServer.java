package com.hetang.maijiayun.report;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 项目启动类
 * @author zbx
 * date : 2017-07-18
 */
public class MainServer {
	
	private final static Logger LOG = Logger.getLogger(MainServer.class);
	public static String SHUTDOWN_HOOK_KEY = "SHUTDOWN_HOOK_KEY";
    private static volatile boolean running = true;
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure(MainServer.class.getClassLoader().getResource("log4j.properties"));
		LOG.fatal("MainServer is starting...");
		try{
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(  
	                new String[] { "spring/springContext.xml" });  
			
	        if ("true".equals(System.getProperty(SHUTDOWN_HOOK_KEY))) {
	            Runtime.getRuntime().addShutdownHook(new Thread() {
	                public void run() {
	                    try {
	                        context.stop();
	                        LOG.info("MainServer stopped!");
	                    } catch (Throwable t) {
	                    	LOG.error(t.getMessage(), t);
	                    }
	                    synchronized (MainServer.class) {
	                        running = false;
	                        MainServer.class.notify();
	                    }
	                }
	            });
	        }
	
	        context.start();
	        LOG.info("MainServer has started");
	
	        synchronized (MainServer.class) {
	            while (running) {
	                try {
	                	MainServer.class.wait();
	                } catch (Throwable e) {}
	            }
	        }
	    } catch(BeansException be) {
	    	LOG.info("MainServer failed : " +be);
	    }catch(Exception e){
	    	LOG.info("MainServer failed : " +e);
	    }
	}
	
}
