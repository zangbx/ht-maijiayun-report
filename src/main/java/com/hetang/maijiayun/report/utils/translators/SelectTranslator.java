package com.hetang.maijiayun.report.utils.translators;

import com.hetang.maijiayun.api.util.APIException;
import com.hetang.maijiayun.api.util.APIExceptionErrorCode;
import com.hetang.maijiayun.api.util.ClassMeta;
import com.hetang.maijiayun.api.util.FieldMeta;

import org.apache.commons.lang.time.FastDateFormat;

import java.lang.reflect.Field;

/**
 * Created by liuyin on 14-6-13.
 */
public class SelectTranslator {

    protected static final FastDateFormat sdf = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");

    public static final String doTranslate(Class<?> cls) throws APIException {

        Field[] fields = cls.getDeclaredFields();
        ClassMeta cm = cls.getAnnotation(ClassMeta.class);
        String selectFieldsClause = new String();

        for (Field f : fields) {
            FieldMeta fm = f.getAnnotation(FieldMeta.class);

            // 没有注解的字段略过
            if (null == fm)
                continue;

            // 非数据库表列略过
            if (fm.column().equals(""))
                continue;

            // 默认取ClassMeta.table作为表名，否则取列上指定的表名
            if (fm.table().equals("")) {
                if (null == cm || cm.table().equals("")) {
                    selectFieldsClause += "`" + fm.column() + "` as '" + f.getName() + "',";
                } else if (!cm.alias().equals("")) {
                    selectFieldsClause += "`" + cm.alias() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                } else {
                    selectFieldsClause += "`" + cm.table() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                }
            } else {
                if (!fm.alias().equals("")) {
                    selectFieldsClause += "`" + fm.alias() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                } else {
                    selectFieldsClause += "`" + fm.table() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                }
            }
        }

        if (!selectFieldsClause.equals("")) {
            return selectFieldsClause.substring(0, selectFieldsClause.length() - 1);
        } else {
            return null;
        }
    }
    public static final String doTranslate(String fieldNames[],Class<?> cls) throws APIException {

        if(fieldNames==null||fieldNames.length==0){
            throw new APIException(APIExceptionErrorCode.U_INTERNAL_PROGRAM_ERROR, "传入的字段列表为空"+cls.getName());
        }
        ClassMeta cm = cls.getAnnotation(ClassMeta.class);
        String selectFieldsClause = new String();
        
        for (String fieldName : fieldNames) {
            Field f;
            try {
                f = cls.getDeclaredField(fieldName);
            } catch (SecurityException e) {
                e.printStackTrace();
                throw new APIException(APIExceptionErrorCode.U_INTERNAL_PROGRAM_ERROR, cls.getName()+"非法的字段:"+fieldName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
                throw new APIException(APIExceptionErrorCode.U_INTERNAL_PROGRAM_ERROR, cls.getName()+"非法的字段:"+fieldName);
            }
            FieldMeta fm = f.getAnnotation(FieldMeta.class);

            // 没有注解的字段略过
            if (null == fm)
                continue;

            // 非数据库表列略过
            if (fm.column().equals(""))
                continue;

            // 默认取ClassMeta.table作为表名，否则取列上指定的表名
            if (fm.table().equals("")) {
                if (null == cm || cm.table().equals("")) {
                    selectFieldsClause += "`" + fm.column() + "` as '" + f.getName() + "',";
                } else if (!cm.alias().equals("")) {
                    selectFieldsClause += "`" + cm.alias() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                } else {
                    selectFieldsClause += "`" + cm.table() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                }
            } else {
                if (!fm.alias().equals("")) {
                    selectFieldsClause += "`" + fm.alias() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                } else {
                    selectFieldsClause += "`" + fm.table() + "`.`" + fm.column() + "` as '" + f.getName() + "',";
                }
            }
        }

        if (!selectFieldsClause.equals("")) {
            return selectFieldsClause.substring(0, selectFieldsClause.length() - 1);
        } else {
            return null;
        }
    }
}
