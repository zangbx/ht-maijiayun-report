package com.hetang.maijiayun.report.utils;    
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;    
public class DynamicDataSource extends AbstractRoutingDataSource {    
	
	Logger logger = Logger.getAnonymousLogger();  
	
    @Override    
    protected Object determineCurrentLookupKey() {    
        return CustomerContextHolder.getCustomerType();    
    }

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return logger;
	}    
}    