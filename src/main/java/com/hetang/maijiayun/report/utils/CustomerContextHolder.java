package com.hetang.maijiayun.report.utils;

public class CustomerContextHolder {
	public static final String DATA_SOURCE_ETL = "etlDataSource";
	public static final String DATA_SOURCE_SLAVE = "slaveDataSource";
	
	private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
	
	public static void setCustomerType(String customerType) {
		contextHolder.set(customerType);
	}

	public static String getCustomerType() {
		return contextHolder.get();
	}

	public static void clearCustomerType() {
		contextHolder.remove();
	}
}