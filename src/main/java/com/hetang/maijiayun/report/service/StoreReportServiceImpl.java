package com.hetang.maijiayun.report.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.hetang.maijiayun.api.report.IStoreReportService;
import com.hetang.maijiayun.api.report.model.StoreSaleReport;
import com.hetang.maijiayun.api.report.model.StoreSkuSaleReport;
import com.hetang.maijiayun.api.util.APIException;
import com.hetang.maijiayun.api.util.Page;
import com.hetang.maijiayun.api.util.PageResultSet;
import com.hetang.maijiayun.report.mappers.StoreReportMapper;
import com.hetang.maijiayun.report.utils.CustomerContextHolder;
import com.hetang.maijiayun.utils.security.SecuritySession;

/**
 * 商品销售报表
 * 
 * @author yangfan
 */
@Service("storeReportService")
public class StoreReportServiceImpl implements IStoreReportService {
	@Autowired
	StoreReportMapper storeReportMapper;

	public static final Logger logger = LoggerFactory.getLogger(StoreReportServiceImpl.class);

	@Override
	public PageResultSet<StoreSaleReport> getStoreSale(JSONObject param, SecuritySession session) throws APIException {
		if (Objects.isNull(param)) {
			throw APIException.builder().illegalArgError().errorMsg("查询参数不能为空").build();
		}
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_ETL);
		param.put("tenantryId", session.tenantry.id);
		// 根据分页参数生成limit所需数据
		Integer pageNo = param.getInteger("pageNo") == null ? 1 : param.getInteger("pageNo");
		Integer pageSize = param.getInteger("pageSize") == null ? 50 : param.getInteger("pageSize");

		// 查询总条数
		Integer totalNum = storeReportMapper.countStoreSale(param);
		if(totalNum == 0){
			return new PageResultSet<StoreSaleReport>(new Page(pageNo, pageSize), 0, new ArrayList<StoreSaleReport>());
		}

		String pageLimitClause = (pageNo - 1) * pageSize + "," + pageSize;
		param.put("pageLimitClause", pageLimitClause);
		
		List<StoreSaleReport> reportDatas = storeReportMapper.listStoreSale(param);
		return new PageResultSet<StoreSaleReport>(new Page(pageNo,pageSize), totalNum, reportDatas);
	}

	@Override
	public StoreSaleReport getSumStoreSale(JSONObject param, SecuritySession session) throws APIException {
		if (Objects.isNull(param)) {
			throw APIException.builder().illegalArgError().errorMsg("查询参数不能为空").build();
		}
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_ETL);
		param.put("tenantryId", session.tenantry.id);
		StoreSaleReport sumStoreSale = storeReportMapper.getSumStoreSale(param);
		if(Objects.isNull(sumStoreSale)){
			sumStoreSale = new StoreSaleReport();
		}
		return sumStoreSale;
	}
	
	@Override
	public PageResultSet<StoreSkuSaleReport> getStoreSkuSale(JSONObject param, SecuritySession session) throws APIException {
		if (Objects.isNull(param)) {
			throw APIException.builder().illegalArgError().errorMsg("查询参数不能为空").build();
		}
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_ETL);
		param.put("tenantryId", session.tenantry.id);
		// 根据分页参数生成limit所需数据
		Integer pageNo = param.getInteger("pageNo") == null ? 1 : param.getInteger("pageNo");
		Integer pageSize = param.getInteger("pageSize") == null ? 50 : param.getInteger("pageSize");

		// 查询总条数
		Integer totalNum = storeReportMapper.countStoreSkuSale(param);
		if(totalNum == 0){
			return new PageResultSet<StoreSkuSaleReport>(new Page(pageNo, pageSize), 0, new ArrayList<StoreSkuSaleReport>());
		}

		String pageLimitClause = (pageNo - 1) * pageSize + "," + pageSize;
		param.put("pageLimitClause", pageLimitClause);
		
		List<StoreSkuSaleReport> reportDatas = storeReportMapper.listStoreSkuSale(param);
		return new PageResultSet<StoreSkuSaleReport>(new Page(pageNo,pageSize), totalNum, reportDatas);
	}

	@Override
	public StoreSkuSaleReport getSumStoreSkuSale(JSONObject param, SecuritySession session) throws APIException {
		if (Objects.isNull(param)) {
			throw APIException.builder().illegalArgError().errorMsg("查询参数不能为空").build();
		}
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_ETL);
		param.put("tenantryId", session.tenantry.id);
		StoreSkuSaleReport sumStoreSkuSale = storeReportMapper.getSumStoreSkuSale(param);
		if(Objects.isNull(sumStoreSkuSale)){
			sumStoreSkuSale = new StoreSkuSaleReport();
		}
		return sumStoreSkuSale;
	}

}
