package com.hetang.maijiayun.report.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.hetang.maijiayun.api.eshop.EShop;
import com.hetang.maijiayun.api.eshop.IEShopService;
import com.hetang.maijiayun.api.report.IOrderSaleReportService;
import com.hetang.maijiayun.api.report.model.OrderSaleReport;
import com.hetang.maijiayun.api.report.model.OrderSaleReportDetail;
import com.hetang.maijiayun.api.report.model.SaleDailyReport;
import com.hetang.maijiayun.api.report.model.SaleMonthReport;
import com.hetang.maijiayun.api.report.model.SkuSaleReportDetail;
import com.hetang.maijiayun.api.util.APIException;
import com.hetang.maijiayun.api.util.AndQuery;
import com.hetang.maijiayun.api.util.OrQuery;
import com.hetang.maijiayun.api.util.Page;
import com.hetang.maijiayun.api.util.PageResultSet;
import com.hetang.maijiayun.report.mappers.OrderSaleReportMapper;
import com.hetang.maijiayun.report.mappers.SkuSaleReportMapper;
import com.hetang.maijiayun.report.utils.CustomerContextHolder;
import com.hetang.maijiayun.utils.security.SecuritySession;

/**
 * 订单销售报表
 * 
 * @author yangfan
 */
@Service("orderSaleReportService")
public class OrderSaleReportServiceImpl implements IOrderSaleReportService {
	@Autowired
	OrderSaleReportMapper orderSaleReportMapper;
	@Autowired
	SkuSaleReportMapper skuSaleReportMapper;
	@Autowired
	IEShopService iEShopService;
	

	public static final Logger logger = LoggerFactory.getLogger(OrderSaleReportServiceImpl.class);

	public PageResultSet<OrderSaleReportDetail> getOrderSaleReportDetail(JSONObject param, SecuritySession session) throws APIException {
		if (Objects.isNull(param)) {
			APIException.builder().illegalArgError().errorMsg("查询参数不能为空").throwExcetion();
		}
		List<OrderSaleReportDetail> reportDatas = new ArrayList<OrderSaleReportDetail>();
		
		// 根据分页参数生成limit所需数据
		Integer pageNo = Objects.isNull(param.getInteger("pageNo")) ? 1 : param.getInteger("pageNo");
		Integer pageSize = param.getInteger("pageSize") == null ? 50 : param.getInteger("pageSize");
		Page page = new Page(pageNo,pageSize);
		
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_SLAVE);
		EShop[] eshops = iEShopService.getList(session.tenantry.id, session.user.id, new AndQuery(), new OrQuery(), "id", true, session);
		if(Objects.isNull(eshops) || eshops.length == 0){
			logger.error("#OrderSaleReportServiceImpl#eshops is null");
			return new PageResultSet<OrderSaleReportDetail>(page, 0, reportDatas);
		}
		fullConditionWithPower(param, eshops, session);//补充相应的参数,租户,以及店铺权限
		
		logger.info("#OrderSaleReportServiceImpl#param:"+param);
		// 查询总条数
		Integer totalNum = orderSaleReportMapper.countOrderSaleReportDetail(param);
		if(totalNum > 0){
			param.put("pageLimitClause", (pageNo - 1) * pageSize + "," + pageSize);
			// 查出选择日期内,每天的统计数据
			reportDatas = orderSaleReportMapper.getOrderSaleReportDetail(param);
			if(CollectionUtils.isNotEmpty(reportDatas)){//补充店铺名称
				Map<String,String> shopCodeShopNameMap = new HashMap<String,String>();
				for (EShop eShop : eshops) {
					shopCodeShopNameMap.put(eShop.getB2cCode(), eShop.getTitle());
				}
				for (OrderSaleReportDetail detail : reportDatas) {
					detail.setEshopName(shopCodeShopNameMap.get(detail.getEshopCode()));
				}
			}
		}
		return new PageResultSet<OrderSaleReportDetail>(page, totalNum, reportDatas);
	}

	/**
	 * 商品销售统计汇总
	 */
	@Override
	public OrderSaleReport getOrderSaleReport(JSONObject param, SecuritySession session) throws APIException {
		if (Objects.isNull(param)) {
			param = new JSONObject();
		}
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_SLAVE);
		EShop[] eshops = iEShopService.getList(session.tenantry.id, session.user.id, new AndQuery(), new OrQuery(), "id", true, session);
		if(Objects.isNull(eshops) || eshops.length == 0){
			return new OrderSaleReport();
		}
		fullConditionWithPower(param, eshops, session);//补充相应的参数,租户,以及店铺权限
		return orderSaleReportMapper.getOrderSaleReport(param);
	}
	
	@Override
	public List<SkuSaleReportDetail> getSkuSaleReportDetailByOrderId(JSONObject param, SecuritySession session) throws APIException {
		if(Objects.isNull(param) || Objects.isNull(param.getLong("orderId"))){
			APIException.builder().illegalArgError().errorMsg("查询参数不能为空").throwExcetion();
		}
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_SLAVE);
		param.put("tenantryId", session.tenantry.id);
		return skuSaleReportMapper.getSkuSaleReportDetail(param);
	}
	
	private void fullConditionWithPower(JSONObject param, EShop[] eshops, SecuritySession session){
		param.put("tenantryId", session.tenantry.id);
		JSONArray eshopCodeArr = param.getJSONArray("eshopCodes");
		// 增加店铺权限 FIXME 前端已经过滤过,如果指定了店铺,这里无需比较了 当然如果数据权限要求特别严格,这里还是在比较下
		if(CollectionUtils.isEmpty(eshopCodeArr) && !session.user.isAdmin){
			List<String> eshopCodes = new ArrayList<String>();
			for (EShop eShop : eshops) {
				eshopCodes.add(eShop.getB2cCode());
			}
			param.put("eshopCodes", eshopCodes);
		}
	}

	@Override
	public List<SaleDailyReport> getSaleDailyReport(JSONObject param, SecuritySession session) throws APIException {
		if(Objects.isNull(param) || StringUtils.isBlank(param.getString("reportDate"))){
			APIException.builder().illegalArgError().errorMsg("日期不能为空").throwExcetion();
		}
		EShop[] eshops = iEShopService.getList(session.tenantry.id, session.user.id, new AndQuery(), new OrQuery(), "id", true, session);
		if(Objects.isNull(eshops) || eshops.length == 0){
			return new ArrayList<SaleDailyReport>();
		}
		fullConditionWithPower(param, eshops, session);
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_ETL);
		List<SaleDailyReport> saleDailyReportList = orderSaleReportMapper.getSaleDailyReport(param);
		//计算出平台
		Map<String,List<SaleDailyReport>> b2cNameSaleDailyReportMap = new HashMap<String,List<SaleDailyReport>>();
		for (SaleDailyReport item : saleDailyReportList) {
			if(StringUtils.isBlank(item.getEshopCode())){
				continue;
			}
			if(item.getEshopCode().contains("|")){
				String b2cIdentifier = item.getEshopCode().split("\\|")[0];
				String b2cName = EShop.b2cIdentifierNameMap.get(b2cIdentifier) == null ? "其他" : EShop.b2cIdentifierNameMap.get(b2cIdentifier);
				if(b2cNameSaleDailyReportMap.containsKey(b2cName)){
					b2cNameSaleDailyReportMap.get(b2cName).add(item);
				}else{
					b2cNameSaleDailyReportMap.put(b2cName, Lists.newArrayList(item));
				}
			}else{
				if(b2cNameSaleDailyReportMap.containsKey("其他")){
					b2cNameSaleDailyReportMap.get("其他").add(item);
				}else{
					b2cNameSaleDailyReportMap.put("其他", Lists.newArrayList(item));
				}
			}
		}
		List<SaleDailyReport> ret = new ArrayList<SaleDailyReport>();
		for (Entry<String, List<SaleDailyReport>> entry : b2cNameSaleDailyReportMap.entrySet()) {
			String b2cName = entry.getKey();
			List<SaleDailyReport> reports = entry.getValue();
			SaleDailyReport b2cReport = new SaleDailyReport();
			ret.add(b2cReport);
			b2cReport.setB2cName(b2cName);
			BigDecimal costAmt = BigDecimal.ZERO;
			BigDecimal saleAmt = BigDecimal.ZERO;
			BigDecimal refundAmt = BigDecimal.ZERO;
			BigDecimal grossAmt = BigDecimal.ZERO;
			Integer customerNum = 0;
			Integer orderNum = 0;
			Integer returnOrderNum = 0;
			for (SaleDailyReport eshopReport : reports) {
				ret.add(eshopReport);
				costAmt = costAmt.add(eshopReport.getCostAmt());
				saleAmt = saleAmt.add(eshopReport.getSaleAmt());
				grossAmt = grossAmt.add(eshopReport.getGrossAmt());
				orderNum += eshopReport.getOrderNum();
				returnOrderNum += eshopReport.getReturnOrderNum();
				refundAmt = refundAmt.add(eshopReport.getRefundAmt());
				customerNum += eshopReport.getCustomerNum();
			}
			b2cReport.setCostAmt(costAmt);
			b2cReport.setCustomerNum(customerNum);
			b2cReport.setGrossAmt(grossAmt);
			b2cReport.setOrderNum(orderNum);
			b2cReport.setSaleAmt(saleAmt);
			b2cReport.setReturnOrderNum(returnOrderNum);
			b2cReport.setRefundAmt(refundAmt);
			if(customerNum != 0){
				b2cReport.setAvgCustomerAmt(saleAmt.divide(new BigDecimal(customerNum), 2,BigDecimal.ROUND_HALF_UP));
			}else{
				b2cReport.setAvgCustomerAmt(BigDecimal.ZERO);
			}
			if(BigDecimal.ZERO.compareTo(saleAmt) != 0){
				b2cReport.setGrossRatio(grossAmt.divide(saleAmt, 4,BigDecimal.ROUND_HALF_UP));
				b2cReport.setRefundAmtRatio(refundAmt.divide(saleAmt, 4,BigDecimal.ROUND_HALF_UP));
			}else{
				b2cReport.setGrossRatio(BigDecimal.ZERO);
				b2cReport.setRefundAmtRatio(BigDecimal.ZERO);
			}
		}
		return ret;
	}

	@Override
	public List<SaleMonthReport> getSaleMonthReport(JSONObject param, SecuritySession session) throws APIException {
		if(Objects.isNull(param) || StringUtils.isBlank(param.getString("reportDate"))){
			APIException.builder().illegalArgError().errorMsg("日期不能为空").throwExcetion();
		}
		EShop[] eshops = iEShopService.getList(session.tenantry.id, session.user.id, new AndQuery(), new OrQuery(), "id", true, session);
		if(Objects.isNull(eshops) || eshops.length == 0){
			return new ArrayList<SaleMonthReport>();
		}
		fullConditionWithPower(param, eshops, session);
		CustomerContextHolder.setCustomerType(CustomerContextHolder.DATA_SOURCE_ETL);
		List<SaleMonthReport> saleMonthReportList = orderSaleReportMapper.getSaleMonthReport(param);
		//计算出平台
		Map<String,List<SaleMonthReport>> b2cNameSaleMonthReportMap = new HashMap<String,List<SaleMonthReport>>();
		for (SaleMonthReport item : saleMonthReportList) {
			if(StringUtils.isBlank(item.getEshopCode())){
				continue;
			}
			if(item.getEshopCode().contains("|")){
				String b2cIdentifier = item.getEshopCode().split("\\|")[0];
				String b2cName = EShop.b2cIdentifierNameMap.get(b2cIdentifier) == null ? "其他" : EShop.b2cIdentifierNameMap.get(b2cIdentifier);
				if(b2cNameSaleMonthReportMap.containsKey(b2cName)){
					b2cNameSaleMonthReportMap.get(b2cName).add(item);
				}else{
					b2cNameSaleMonthReportMap.put(b2cName, Lists.newArrayList(item));
				}
			}else{
				if(b2cNameSaleMonthReportMap.containsKey("其他")){
					b2cNameSaleMonthReportMap.get("其他").add(item);
				}else{
					b2cNameSaleMonthReportMap.put("其他", Lists.newArrayList(item));
				}
			}
		}
		List<SaleMonthReport> ret = new ArrayList<SaleMonthReport>();
		for (Entry<String, List<SaleMonthReport>> entry : b2cNameSaleMonthReportMap.entrySet()) {
			String b2cName = entry.getKey();
			List<SaleMonthReport> reports = entry.getValue();
			SaleMonthReport b2cReport = new SaleMonthReport();
			ret.add(b2cReport);
			b2cReport.setB2cName(b2cName);
			BigDecimal costAmt = BigDecimal.ZERO;
			BigDecimal saleAmt = BigDecimal.ZERO;
			BigDecimal refundAmt = BigDecimal.ZERO;
			BigDecimal grossAmt = BigDecimal.ZERO;
			Integer customerNum = 0;
			Integer orderNum = 0;
			Integer returnOrderNum = 0;
			for (SaleMonthReport eshopReport : reports) {
				ret.add(eshopReport);
				costAmt = costAmt.add(eshopReport.getCostAmt());
				saleAmt = saleAmt.add(eshopReport.getSaleAmt());
				grossAmt = grossAmt.add(eshopReport.getGrossAmt());
				orderNum += eshopReport.getOrderNum();
				returnOrderNum += eshopReport.getReturnOrderNum();
				refundAmt = refundAmt.add(eshopReport.getRefundAmt());
				customerNum += eshopReport.getCustomerNum();
			}
			b2cReport.setCostAmt(costAmt);
			b2cReport.setCustomerNum(customerNum);
			b2cReport.setGrossAmt(grossAmt);
			b2cReport.setOrderNum(orderNum);
			b2cReport.setSaleAmt(saleAmt);
			b2cReport.setReturnOrderNum(returnOrderNum);
			b2cReport.setRefundAmt(refundAmt);
			if(customerNum != 0){
				b2cReport.setAvgCustomerAmt(saleAmt.divide(new BigDecimal(customerNum), 2,BigDecimal.ROUND_HALF_UP));
			}else{
				b2cReport.setAvgCustomerAmt(BigDecimal.ZERO);
			}
			if(BigDecimal.ZERO.compareTo(saleAmt) != 0){
				b2cReport.setGrossRatio(grossAmt.divide(saleAmt, 4,BigDecimal.ROUND_HALF_UP));
				b2cReport.setRefundAmtRatio(refundAmt.divide(saleAmt, 4,BigDecimal.ROUND_HALF_UP));
			}else{
				b2cReport.setGrossRatio(BigDecimal.ZERO);
				b2cReport.setRefundAmtRatio(BigDecimal.ZERO);
			}
		}
		return ret;
	}

}
