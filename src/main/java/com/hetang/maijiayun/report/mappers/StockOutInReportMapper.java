package com.hetang.maijiayun.report.mappers;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.hetang.maijiayun.api.report.model.StockOutInReport;
import com.hetang.maijiayun.api.report.model.StockOutInReportDetail;

public interface StockOutInReportMapper {

	public Integer countSku(JSONObject param);

	public List<StockOutInReport> fetch(JSONObject param);
	
	public JSONObject getTotalStockOutInNum(JSONObject param);

	public Integer countStockOutInDetail(JSONObject param);

	public List<StockOutInReportDetail> fetchDetail(JSONObject param);
	
}