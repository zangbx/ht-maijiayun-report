package com.hetang.maijiayun.report.mappers;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.hetang.maijiayun.api.report.model.StoreSaleReport;
import com.hetang.maijiayun.api.report.model.StoreSkuSaleReport;

public interface StoreReportMapper {

	//---门店销售统计
	public Integer countStoreSale(JSONObject param);

	public List<StoreSaleReport> listStoreSale(JSONObject param);

	public StoreSaleReport getSumStoreSale(JSONObject param);

	//---门店商品销售统计
	public Integer countStoreSkuSale(JSONObject param);
	
	public List<StoreSkuSaleReport> listStoreSkuSale(JSONObject param);

	public StoreSkuSaleReport getSumStoreSkuSale(JSONObject param);


}