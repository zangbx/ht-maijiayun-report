package com.hetang.maijiayun.report.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.alibaba.fastjson.JSONObject;
import com.hetang.maijiayun.api.report.model.SkuSaleReport;
import com.hetang.maijiayun.api.report.model.SkuSaleReportDetail;
import com.hetang.maijiayun.api.report.model.SkuSaleCustomerReport;

public interface SkuSaleReportMapper {

	public Integer countSkuSaleReportDetail(JSONObject param);

	public List<SkuSaleReportDetail> getSkuSaleReportDetail(JSONObject param);
	
	public SkuSaleReport getSkuSaleReport(JSONObject param);
	
	/**
	 * 根据客户钻取商品销售统计数据
	 * @param param
	 * @return
	 */
	public List<SkuSaleCustomerReport> getSkuSaleCustomerReport(JSONObject param);
	
	public Integer countSkuSaleCustomerReport(JSONObject param);

	public List<JSONObject> getCategorySaleRank(JSONObject param);

	public List<JSONObject> getBrandSaleRank(JSONObject param);
	
	public List<JSONObject> getAreaSaleRank(JSONObject param);
	
	public List<JSONObject> getSkuSaleRank(JSONObject param);
	/**
	 * 获取商品销售统计总金额以及总销售数量
	 * @param param
	 * @return
	 */
	public JSONObject getSumSale(JSONObject param);

	public List<JSONObject> getOrderDailySale(JSONObject param);
	
	public List<Long> getSaleTopSkuIds(@Param("topNum") Integer topNum,@Param("supplierCode")String supplierCode, @Param("tenantryId") Long tenantryId);
	
}