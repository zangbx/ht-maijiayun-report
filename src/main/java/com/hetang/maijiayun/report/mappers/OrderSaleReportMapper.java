package com.hetang.maijiayun.report.mappers;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.hetang.maijiayun.api.report.model.OrderSaleReport;
import com.hetang.maijiayun.api.report.model.OrderSaleReportDetail;
import com.hetang.maijiayun.api.report.model.SaleDailyReport;
import com.hetang.maijiayun.api.report.model.SaleMonthReport;

public interface OrderSaleReportMapper {

	public Integer countOrderSaleReportDetail(JSONObject param);

	public List<OrderSaleReportDetail> getOrderSaleReportDetail(JSONObject param);
	
	public OrderSaleReport getOrderSaleReport(JSONObject param);

	public List<SaleDailyReport> getSaleDailyReport(JSONObject param);

	public List<SaleMonthReport> getSaleMonthReport(JSONObject param);
	
}